module go-work/micro

go 1.15

require (
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/h2non/filetype v1.1.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	google.golang.org/grpc v1.33.2 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
