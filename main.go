package main

import (
	"bytes"
	"fmt"
	"image"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"sync"
	"time"

	"github.com/disintegration/imaging"
)

var (
	height int = 300
	width  int = 0
	wg     sync.WaitGroup
)

func main() {
	http.HandleFunc("/upload", uploadFile)
	http.ListenAndServe(":8080", nil)
}

var ch chan int

func uploadFile(w http.ResponseWriter, r *http.Request) {
	defer wg.Wait()
	r.ParseMultipartForm(10 << 20)

	file, _, err := r.FormFile("myFile")
	defer file.Close()

	if err != nil {
		fmt.Println("Error Retrieving the File")
		return
	}

	wg.Add(2)
	ch := make(chan struct{})
	go uploadStatus(ch)
	go resizeImg(file, ch)
}

func resizeImg(file multipart.File, c chan struct{}) {
	defer wg.Done()
	defer close(c)
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}

	img, _, _ := image.Decode(bytes.NewReader(fileBytes))
	dstimg := imaging.Resize(img, width, height, imaging.Box)
	if err = imaging.Save(dstimg, "./"+time.Now().Format("2006_01_02")+".jpg"); err != nil {
		fmt.Println(err)
	}
	c <- struct{}{}
}

func uploadStatus(c <-chan struct{}) {
	defer wg.Done()
	for {
		select {
		case <-c:
			fmt.Println("Message: Изображение создано.")
			return
		}
	}
}
